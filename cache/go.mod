module cache

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/json-iterator/go v1.1.7
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
	github.com/seaguest/common v0.0.0-20190827075439-162c56973155
)
